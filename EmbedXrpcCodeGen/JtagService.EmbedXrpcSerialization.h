﻿#ifndef JtagService_EmbedXrpcSerialization_H
#define JtagService_EmbedXrpcSerialization_H

//auto code gen ! DO NOT modify this file!
//自动代码生成,请不要修改本文件!

//EmbedXrpc Code gen .By snikeguo.e-mail:snikeguo@foxmail.com
//关于snikeguo作者:92年生人,热爱技术,底层功底扎实.深入理解C语言底层到汇编层，单片机从外设/裸机到OS均有涉猎
//上位机专注于C# 界面库熟悉WPF.服务器专注于dotnet core
//行业经验：汽车电子.独自完成UDS(包括FBL)协议栈  GBT27930-2015(SAE J1939)协议栈等
//熟悉BMS业务逻辑,有一套自己稳定出货的BMS软件/硬件/上位机/服务器/USB转CAN&CANFD
//TITLE:高级嵌入式软件工程师.软件架构师
//微信:snikeguo.有好的职位推荐请加
#ifndef offsetof
#define offsetof(s, m) (size_t)((char*)(&((s*)0)->m))
#endif
extern const UInt32Field DrRes_Field_DrLen;
extern const ArrayField DrRes_Field_OutDr;
extern const IField* DrResDesc [2];
extern const ObjectType DrRes_Type;

extern const UInt32Field IrRes_Field_IrLen;
extern const ArrayField IrRes_Field_OutIr;
extern const IField* IrResDesc [2];
extern const ObjectType IrRes_Type;

extern const UInt32Field Inter_SetSpeed_Request_Field_Speed;
extern const IField* Inter_SetSpeed_RequestDesc [1];
extern const ObjectType Inter_SetSpeed_Request_Type;

#define Inter_SetSpeed_ServiceId 16
extern const UInt8Field Inter_SetSpeed_Response_Field_State;
extern const UInt8Field Inter_SetSpeed_Response_Field_ReturnValue;
extern const IField* Inter_SetSpeed_ResponseDesc [2];
extern const ObjectType Inter_SetSpeed_Response_Type;

extern const UInt32Field Inter_TapShiftDr_Request_Field_DrLen;
extern const ArrayField Inter_TapShiftDr_Request_Field_InDr;
extern const IField* Inter_TapShiftDr_RequestDesc [2];
extern const ObjectType Inter_TapShiftDr_Request_Type;

#define Inter_TapShiftDr_ServiceId 17
extern const UInt8Field Inter_TapShiftDr_Response_Field_State;
extern const ObjectField Inter_TapShiftDr_Response_Field_ReturnValue;
extern const IField* Inter_TapShiftDr_ResponseDesc [2];
extern const ObjectType Inter_TapShiftDr_Response_Type;

extern const UInt32Field Inter_TapShiftIr_Request_Field_IrLen;
extern const ArrayField Inter_TapShiftIr_Request_Field_InIr;
extern const IField* Inter_TapShiftIr_RequestDesc [2];
extern const ObjectType Inter_TapShiftIr_Request_Type;

#define Inter_TapShiftIr_ServiceId 18
extern const UInt8Field Inter_TapShiftIr_Response_Field_State;
extern const ObjectField Inter_TapShiftIr_Response_Field_ReturnValue;
extern const IField* Inter_TapShiftIr_ResponseDesc [2];
extern const ObjectType Inter_TapShiftIr_Response_Type;

extern const ObjectType Inter_TapGoIdle_Request_Type;

#define Inter_TapGoIdle_ServiceId 19
extern const UInt8Field Inter_TapGoIdle_Response_Field_State;
extern const ObjectField Inter_TapGoIdle_Response_Field_ReturnValue;
extern const IField* Inter_TapGoIdle_ResponseDesc [2];
extern const ObjectType Inter_TapGoIdle_Response_Type;

extern const UInt32Field Inter_TapRun_Request_Field_Ticks;
extern const IField* Inter_TapRun_RequestDesc [1];
extern const ObjectType Inter_TapRun_Request_Type;

#define Inter_TapRun_ServiceId 20
extern const UInt8Field Inter_Set_TAP_IR_PRE_Request_Field_IrPre;
extern const IField* Inter_Set_TAP_IR_PRE_RequestDesc [1];
extern const ObjectType Inter_Set_TAP_IR_PRE_Request_Type;

#define Inter_Set_TAP_IR_PRE_ServiceId 21
extern const UInt8Field Inter_Set_TAP_IR_POST_Request_Field_IrPre;
extern const IField* Inter_Set_TAP_IR_POST_RequestDesc [1];
extern const ObjectType Inter_Set_TAP_IR_POST_Request_Type;

#define Inter_Set_TAP_IR_POST_ServiceId 22
extern const UInt8Field Inter_Set_TAP_DR_PRE_Request_Field_IrPre;
extern const IField* Inter_Set_TAP_DR_PRE_RequestDesc [1];
extern const ObjectType Inter_Set_TAP_DR_PRE_Request_Type;

#define Inter_Set_TAP_DR_PRE_ServiceId 23
extern const UInt8Field Inter_Set_TAP_DR_POST_Request_Field_IrPre;
extern const IField* Inter_Set_TAP_DR_POST_RequestDesc [1];
extern const ObjectType Inter_Set_TAP_DR_POST_Request_Type;

#define Inter_Set_TAP_DR_POST_ServiceId 24
extern const ObjectType Inter_Reset_Request_Type;

#define Inter_Reset_ServiceId 25

#endif
