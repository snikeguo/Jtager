﻿#ifndef JtagService_H
#define JtagService_H
#include"EmbedSerialization.h"
#include"EmbedXrpcCommon.h"

//auto code gen ! DO NOT modify this file!
//自动代码生成,请不要修改本文件!

typedef struct _DrRes
{
UInt32 DrLen;
UInt32 OutDr[4];
}DrRes;
typedef struct _IrRes
{
UInt32 IrLen;
UInt32 OutIr[4];
}IrRes;
typedef struct _Inter_SetSpeed_Request
{
UInt32 Speed;
}Inter_SetSpeed_Request;
typedef struct _Inter_SetSpeed_Response
{
ResponseState State;
Boolean ReturnValue;
}Inter_SetSpeed_Response;
typedef struct _Inter_TapShiftDr_Request
{
UInt32 DrLen;
UInt32 InDr[4];
}Inter_TapShiftDr_Request;
typedef struct _Inter_TapShiftDr_Response
{
ResponseState State;
DrRes ReturnValue;
}Inter_TapShiftDr_Response;
typedef struct _Inter_TapShiftIr_Request
{
UInt32 IrLen;
UInt32 InIr[4];
}Inter_TapShiftIr_Request;
typedef struct _Inter_TapShiftIr_Response
{
ResponseState State;
IrRes ReturnValue;
}Inter_TapShiftIr_Response;
typedef struct _Inter_TapGoIdle_Request
{
}Inter_TapGoIdle_Request;
typedef struct _Inter_TapGoIdle_Response
{
ResponseState State;
IrRes ReturnValue;
}Inter_TapGoIdle_Response;
typedef struct _Inter_TapRun_Request
{
UInt32 Ticks;
}Inter_TapRun_Request;
typedef struct _Inter_Set_TAP_IR_PRE_Request
{
Byte IrPre;
}Inter_Set_TAP_IR_PRE_Request;
typedef struct _Inter_Set_TAP_IR_POST_Request
{
Byte IrPre;
}Inter_Set_TAP_IR_POST_Request;
typedef struct _Inter_Set_TAP_DR_PRE_Request
{
Byte IrPre;
}Inter_Set_TAP_DR_PRE_Request;
typedef struct _Inter_Set_TAP_DR_POST_Request
{
Byte IrPre;
}Inter_Set_TAP_DR_POST_Request;
typedef struct _Inter_Reset_Request
{
}Inter_Reset_Request;

#endif
