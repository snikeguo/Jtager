using System;
using EmbedXrpcIdlParser;

[FileName("Jtager.cs")]
struct DrRes
{
    UInt32 DrLen;
    [MaxCount(IsFixed=true, MaxCount =4,LenFieldName = "DrLen")]
    UInt32 []OutDr;
}
[FileName("Jtager.cs")]
struct IrRes
{
    UInt32 IrLen;
    [MaxCount(IsFixed=true, MaxCount =4,LenFieldName = "IrLen")]
    UInt32 [] OutIr;
}
[FileName("Jtager.cs")]
interface Inter
{
    bool SetSpeed(UInt32 Speed);
    DrRes TapShiftDr(UInt32 DrLen,
    [MaxCount(IsFixed = true, MaxCount = 4, LenFieldName = "DrLen")]
    UInt32[] InDr);
    
    IrRes TapShiftIr(UInt32 IrLen,
    [MaxCount(IsFixed = true, MaxCount = 4, LenFieldName = "IrLen")]
    UInt32[] InIr);

    IrRes TapGoIdle();

    void TapRun(UInt32 Ticks);

    void Set_TAP_IR_PRE(byte IrPre);
    void Set_TAP_IR_POST(byte IrPre);
    void Set_TAP_DR_PRE(byte IrPre);
    void Set_TAP_DR_POST(byte IrPre);

    void Reset();
}
[FileName("Jtager.cs")]
public class OptionProcess:IOptionProcess
{
    public GenerationOption Process()
    {
        GenerationOption option = new GenerationOption();
        option.OutPutFileName = "JtagService";
        option.CSharpNameSpace = "JtagService";
        return option;
    }
}