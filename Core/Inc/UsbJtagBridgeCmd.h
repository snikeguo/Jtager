/*
 * PcMcuCmd.h
 *
 *  Created on: Mar 9, 2020
 *      Author: Administrator
 */

#ifndef INC_USBJTAGBRIDGECMD_H_
#define INC_USBJTAGBRIDGECMD_H_

#define PcMcuCmd_Nrc_SpeedToLow			0x1
#define PcMcuCmd_Nrc_SpeedToHigh		0x2

#define PcMcuCmd_Nrc_Unknow				0xff

/*
 * 协议格式参照ISO14229 UDS的子服务协议规范
 * */
/*
 *	pc->mcu:0x1,speed
 *	正响应 :mcu->pc:0x41,
 *	负响应:mcu->pc:0x7F,PcMcuCmd_Nrc_SpeedToLow/PcMcuCmd_Nrc_SpeedToHigh
 * */
#define PcMcuCmd_Jtag_SetSpeed			0x1

/*
 * pc->mcu 0x2,dr_len in_dr
 * 正响应 :mcu->pc 0x42,dr_len out_dr
 * 负响应:mcu->pc:0x7F,...
 * */

#define PcMcuCmd_Jtag_TapShiftDr 		0x2

/*
 * pc->mcu 0x3,dr_len in_ir
 * 正响应 :mcu->pc 0x43,dr_len out_ir
 * 负响应:mcu->pc:0x7F,...
 * */


#define PcMcuCmd_Jtag_TapShiftIr 		0x3

/*
 * pc->mcu 0x4
 * 正响应 :mcu->pc 0x44,dr_len out_ir
 * 负响应:mcu->pc:0x7F,...
 * */

#define PcMcuCmd_Jtag_TapGoIdle		0x4

/*
 * pc->mcu 0x5  ticks
 * 正响应 :mcu->pc 0x45
 * 负响应:mcu->pc:0x7F,...
 * */
#define PcMcuCmd_Jtag_TapRun			0x5

/*
 * pc->mcu 0x6  IrPre
 * 正响应 :mcu->pc 0x46
 * 负响应:mcu->pc:0x7F,...
 * */
#define PcMcuCmd_Jtag_TapSetIrPre		0x6

/*
 * pc->mcu 0x7  IrPost
 * 正响应 :mcu->pc 0x47
 * 负响应:mcu->pc:0x7F,...
 * */
#define PcMcuCmd_Jtag_TapSetIrPost  	0x7

/*
 * pc->mcu 0x8  DrPre
 * 正响应 :mcu->pc 0x48
 * 负响应:mcu->pc:0x7F,...
 * */
#define PcMcuCmd_Jtag_TapSetDrPre  	0x8

/*
 * pc->mcu 0x9  DrPre
 * 正响应 :mcu->pc 0x49
 * 负响应:mcu->pc:0x7F,...
 * */
#define PcMcuCmd_Jtag_TapSetDrPost  	0x9

#endif /* INC_USBJTAGBRIDGECMD_H_ */
