#include <rtthread.h>
#include <cstdint>
#include <IJtagPort.h>
#include <Jtag.h>
#include <UsbJtagBridge.h>
#include <main.h>

static void JtagTask(void *arg)
{
	uint32_t out[1],in[1];
	in[0]=0x01;
	out[0]=0xAA;
	Jtager.Init();
	//Jtager.SetSpeed(1000);
	//UsbJtagBridger.Init(&Jtager,nullptr,0,nullptr);
    for(;;)
    {
    	Jtager.TapGoIdle();

    	in[0]=0x01;
		Jtager.TapShiftIr(out, in, 5);
		in[0]=0x00;
		Jtager.TapShiftDr(out, in, 32);
    	rt_thread_delay(10);

        HAL_GPIO_TogglePin(LED0_GPIO_Port,LED0_Pin);
    }
}




#ifdef __cplusplus
extern "C" {
#endif

void rt_application_init(void)
{
	void rt_components_init(void);
	rt_components_init();//组件初始化

    rt_thread_t t=rt_thread_create("jtag test task",JtagTask,NULL,4096,10,10);
    rt_thread_startup(t);
}

void DebugSystemInitForC(void)
{
    extern int rtthread_startup();
    rtthread_startup();
}


#ifdef __cplusplus
}
#endif
