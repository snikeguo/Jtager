#include <rtthread.h>
#include <cstdint>
#include <IJtagPort.h>
#include <Jtag.h>
#include <UsbJtagBridge.h>
UsbJtagBridge UsbJtagBridger;
static IJtagService* GetJtagServiceHandle(rt_uint8_t cmdid,IJtagService* services,rt_uint16_t service_len)
{
    rt_uint16_t i=0;
    for(i=0;i<service_len;i++)
    {
        if(cmdid==services[i].GetCmdId())
        {
            return &services[i];
        }
    }
    return nullptr;
}
void UsbJtagBridge::UsbJtagBridgeTask(void *arg)
{
    UsbJtagBridge *bridge=static_cast<UsbJtagBridge *>(arg);
    IJtagService *currentService=nullptr;
    JtagServiceRequest Request;
    for(;;)
    {
        rt_mq_recv(bridge->jtagServiceDataMq,&Request,sizeof(JtagServiceRequest),RT_WAITING_FOREVER);

        rt_mutex_take(bridge->processMutex,RT_WAITING_FOREVER);
        currentService=GetJtagServiceHandle(Request.CmdId,bridge->services,bridge->servicesLen);
        RT_ASSERT(currentService!=RT_NULL);
        currentService->ServerProcessRequest(bridge,&Request);
        rt_free(Request.Data);
        rt_mutex_release(bridge->processMutex);
    }
}
