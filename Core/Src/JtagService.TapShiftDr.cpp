#include <rtthread.h>
#include <cstdint>
#include <IJtagPort.h>
#include <Jtag.h>
#include <UsbJtagBridge.h>
#include <UsbJtagBridgeCmd.h>
class JtagTapShiftDrService:public IJtagService
{
public:
    rt_uint8_t GetCmdId()
    {
        return PcMcuCmd_Jtag_TapShiftDr;
    }
    const char *GetServiceName()
    {
        return "JtagTapShiftDrService";
    }
    void ServerProcessRequest(UsbJtagBridge *bridger,JtagServiceRequest *req)
    {
        Jtag *jtager=bridger->GetJtager();
        rt_uint32_t dr_len=(rt_uint32_t)(req->Data[0]|req->Data[1]<<8|req->Data[2]<<16|req->Data[3]<<24);//小端模式
        rt_uint32_t *in_dr=(rt_uint32_t *)&req->Data[4];
        rt_uint8_t response[64]={PcMcuCmd_Jtag_TapShiftDr+0x40,
        		(rt_uint8_t)(dr_len>>0&0xFF),
				(rt_uint8_t)(dr_len>>8&0xFF),
				(rt_uint8_t)(dr_len>>16&0xFF),
				(rt_uint8_t)(dr_len>>24&0xFF)};
        jtager->TapShiftDr((uint32_t *)&response[2], (uint32_t *)in_dr, dr_len);
        bridger->SendResponse(response,2+dr_len);
    }
};

