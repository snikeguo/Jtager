### 单片机实现的USB转JTAG调试器
本项目旨在实现一个USB转JTAG调试器，使用的是C++.但是由于嵌入式条件限制，仅仅使用了C with Class功能。
***
1.类`IJtagPort`是用GPIO模拟JTAG时序用到的最低级的类，JTAG这一部分只需要移植这个类。
2.`Stm32F103JtagPortImpl`是在单片机STM32F103上的`IJtagPort`实现。
3.`IJtagPort`会被`Jtag`调用，`Jtag`将会实现JTAG时序，并提供一些API接口。
4.`UsbJtagBridge`是连接USB和JTAG的类，也就是说`Jtag`是可以单独使用的，可以直接调用`Jtag`的API，通常者用来测试JTAG时序。`UsbJtagBridgeCmd.h`是USB和JTAG交互的命令，当USB接收到数据后，通过调用`UsbJtagBridge`的接口API，然后会激活`UsbJtagBridgeTask`任务，该任务会查询当前所支持的`IJtagService`接口的子类,进而调用接口子类的`ServerProcessRequest`方法，达到执行JTAG的功能。